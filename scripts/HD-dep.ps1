﻿<#
.SYNOPSIS
    PowerShell installation script for basic HD PS Modules. 
.DESCRIPTION
    PowerShell installation script for basic HD PS Modules.
.NOTES
    Authors  : Ian S. Pringle

    Changelog:
        - 0.0.1 (2018/06/19) Initial version
#>

#variables
$SSignOn = "https://download.microsoft.com/download/7/1/E/71EF1D05-A42C-4A1F-8162-96494B5E615C/msoidcli_64bit.msi"
$Version = "0.0.1"
$wid = [System.Security.Principal.WindowsIdentity]::GetCurrent()
$prp = New-Object System.Security.Principal.WindowsPrincipal($wid)
$adm = [System.Security.Principal.WindowsBuiltInRole]::Administrator
$isAdmin = $prp.isinrole($adm)

#Adds the Windows form Assembly
#Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName PresentationFramework

function Check-ProgramInstalled($progName) 
{
    $x86 = ((Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall") |
        Where-Object { $_.GetValue( "DisplayName" ) -like "*$progName*" } ).Length -gt 0;
    $x64 = ((Get-ChildItem "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall") |
        Where-Object { $_.GetValue( "DisplayName" ) -like "*$progName*" } ).Length -gt 0;
    return $x86 -or $x64;
}

function Install-ClickOnce {
[CmdletBinding()] 
Param(
    $Manifest = "https://cmdletpswmodule.blob.core.windows.net/exopsmodule/Microsoft.Online.CSE.PSModule.Client.application",
    #AssertApplicationRequirements
    $ElevatePermissions = $true
)
    Try { 
        Add-Type -AssemblyName System.Deployment
        
        Write-Verbose "Start installation of ClickOnce Application $Manifest "

        $RemoteURI = [URI]::New( $Manifest , [UriKind]::Absolute)
        if (-not  $Manifest)
        {
            throw "Invalid ConnectionUri parameter '$ConnectionUri'"
        }

        $HostingManager = New-Object System.Deployment.Application.InPlaceHostingManager -ArgumentList $RemoteURI , $False
    
        #register an event to trigger custom event (yep, its a hack)
        Register-ObjectEvent -InputObject $HostingManager -EventName GetManifestCompleted -Action { 
            new-event -SourceIdentifier "ManifestDownloadComplete"
        } | Out-Null
        #register an event to trigger custom event (yep, its a hack)
        Register-ObjectEvent -InputObject $HostingManager -EventName DownloadApplicationCompleted -Action { 
            new-event -SourceIdentifier "DownloadApplicationCompleted"
        } | Out-Null

        #get the Manifest
        $HostingManager.GetManifestAsync()

        #Waitfor up to 5s for our custom event
        $event = Wait-Event -SourceIdentifier "ManifestDownloadComplete" -Timeout 5
        if ($event ) {
            $event | Remove-Event
            Write-Verbose "ClickOnce Manifest Download Completed"

            $HostingManager.AssertApplicationRequirements($ElevatePermissions)
            #todo :: can this fail ?
            
            #Download Application
            $HostingManager.DownloadApplicationAsync()
            #register and wait for completion event
            # $HostingManager.DownloadApplicationCompleted
            $event = Wait-Event -SourceIdentifier "DownloadApplicationCompleted" -Timeout 15
            if ($event ) {
                $event | Remove-Event
                Write-Verbose "ClickOnce Application Download Completed"
            } else {
                Write-error "ClickOnce Application Download did not complete in time (15s)"
            }
        } else {
           Write-error "ClickOnce Manifest Download did not complete in time (5s)"
        }

        #Clean Up
    } finally {
        #get rid of our eventhandlers
        Get-EventSubscriber|? {$_.SourceObject.ToString() -eq 'System.Deployment.Application.InPlaceHostingManager'} | Unregister-Event
    }
}

function InstallerCode
{
    Write-Host "Downloading dependecies..."
    if (!(Check-ProgramInstalled("Microsoft Online Services Sign-in Assistant")))
    {
        Write-Host "Downloading Microsoft Online SSI..."
        New-Item -ItemType directory -Path "C:\\temp\"
        $webclient = New-Object System.Net.WebClient
        $webclient.Headers.Add("User-Agent: Other")    
        $webclient.DownloadFile($SSignOn, "c:\temp\msoidcli_64bit.msi")
        while (!(Test-Path "c:\temp\msoidcli_64bit.msi")) 
            {Start-Sleep 10}
        Write-Host "Installing Microsoft Online SSI..."

        $exeApp = "c:\windows\system32\msiexec.exe"
        $exeArg = "/i c:\temp\msoidcli_64bit.msi ACCEPTEULA=YES /qr+"
        $process = [Diagnostics.Process]::Start($exeApp, $exeArg)
        $process.WaitforExit()
        Write-Host "Installation completed." 
    }
    else
    {
        Write-Host "Microsoft Online Services Sign-in Assistant is already installed."
    }
    Write-Host "Installing Powershell modules..."
    Write-Host "Installing NuGet."
    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
    Write-Host "Adding PSGallery to trusted installation repository."
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
    Write-Host "Installing Microsoft Services Online module."
    install-Module -name msOnline -SkipPublisherCheck
    if (!(Check-ProgramInstalled("Microsoft Azure Active Directory Module for Windows Powershell")))
    {
        Write-Host "Installing Microsoft Azure Active Directory Module for Windows Powershell."
        install-module -Name AzureAD -Confirm:$false
    }
    else
    {
        Write-Host "Microsoft Azure Active Directory Module for Windows Powershell is already installed."
    }
    Write-Host "Adding Powershell specific PATH variables"
    $Old_Path=(Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name Path).Path #Save old Path variable value
    Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value ($Old_Path += ';c:\Program Files\WindowsPowerShell\Scripts') #Append new path to existing path variable

    Write-Host "Installing Exchange MFA Module"
    Install-ClickOnce

}

<#This script can be embedded into other scripts to ensure dependencies are met by including:

(new-object net.webclient).DownloadFile('https://bitbucket.org/ipringle2/ps-tools/raw/master/scripts/HD-dep.ps1','HD-dep.ps1')
./HD-dep.ps1

#>

InstallerCode