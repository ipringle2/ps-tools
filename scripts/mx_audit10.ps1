﻿<#
    Script: mx_audit
    Author: ???
    Modified: Ian S. Pringle
    - 8.6.b Disabled account added to script. Cleared syntax on loops.
    - 8.6.c Pointed ActiveDirectory library to DC05 on Set-ADAccountPassword function.
    - 9.0   Added Auto post to UserManager.  Recoded Logic.  
    - 9.1   Corrected Exchange session code.
    - 9.2   Corrected Session code to verify Username.  Corrected code to remove rules and display all inbox rules. 
    - 9.3   Added expire of account on lock-down procedure.
    - 9.4   Change code and remove try/catch.  Added AssemblyType. Change Exchange connection script.
    - 9.5   Corrected try/catch to remove error on Lock-down code.
    - 9.6   Added Security group correction against user loading script.
    - 10.0.0  Updated to the MFA Exchange Online PS Module, removed the User Manager note, allowed for lock-down prompt regardless of actions taken on account
              Introducing new versioning to allow for updates due to refactoring, as current codebase is in need of cleanup
    Notes:  
    1. If issues happen with script not connecting to Exchange Online, make sure that computer is in the correct OU.  
       Check WinRM for information 
       command prompt to "winrm get winrm/config -format:pretty"
       if Basic authentication is set to False or control by (GPO) then Script will not work.

    2. If the script does not work check execution policy 
       on powershell "get-executionpolicy -list
       LocalMachine should be Unrestricted,
       if not then run the following command
       "Set-ExecutionPolicy -scope LocalMachine unrestricted"
#>

#region function MX_Audit
function MX_Audit
{
    #$username = $Credential.username
    $MXUsername = ""
    $MXMod = 0

    While ($MXUsername -ne "x")
    {
        clear
        Write-Host "MX-Audit $MXAuditVer`n"
        $MXUsername = Read-Host "`nPlease enter the username of the account"
        if ([string]::IsNullOrWhiteSpace($MXUsername))
        {
            #empty username
            Write-Host -ForegroundColor Red "ERROR: Username can not be empty."
        }
        else
        {
            #run script
            if ($MXUsername -ne "x")
            {
                $LDAPUsername = Get-ADUser -LDAPFilter "(sAMAccountName=$MXUsername)"
                if ($LDAPUsername -eq $null)
                    {Write-Host -ForegroundColor Red "ERROR: Username not found in LDAP."}
                else
                {
                    $q = Get-Mailbox $MXUsername -ErrorAction SilentlyContinue
        	        Write-Host "**************************"
                    Write-Host "Retrieving Mailbox Stats" 
                    $OnlineUsername = $MXUsername + "@liberty.edu"
                    $MXOWAPolicy = (Get-CASMailbox -identity $OnlineUsername | Select OWAEnabled)
                    if ($MXOWAPolicy.OWAEnabled -eq $True)
                        {Write-Host "OWA WebAcess: Enabled"}
                    else
                        {Write-Host "OWA WebAcess: Disabled"}
                    $MXSendSize = (Get-Mailbox -identity $OnlineUsername | select MaxSendSize)
                    $MXRec = Get-Mailbox $OnlineUsername | Get-MailboxStatistics | Select DeletedItemCount, ItemCount, TotalDeletedItemSize, TotalItemSize, DisplayName, LastLogonTime

                    $tempStr = $MXRec.DisplayName
                    Write-Host "Display Name: $tempStr" 
                    $tempStr = $MXRec.ItemCount 
                    Write-Host "Total Item count: $tempStr"
                    $tempStr =  $MXRec.TotalItemSize
                    Write-Host "Total Item size: $tempStr" 
                    $tempStr = $MXRec.DeletedItemCount
                    Write-Host "Total Deleted count: $tempStr" 
                    $tempStr = $MXRec.TotalDeletedItemSize
                    Write-Host "Total Deleted size: $tempStr" 
                    $tempStr = $MXRec.LastLogonTime
                    Write-Host "Last Logon Time: $tempStr" 
                    Write-Host "**************************" 
	                Write-Host "Send Block" 
                    $MXSendSize = (Get-Mailbox -identity $OnlineUsername | select MaxSendSize)
                    $tempStr = $MXSendSize.MaxSendSize
                    Write-Host "Max Send Size: $tempStr" 
                    #$MXBlock = (get-mailbox -identity $OnlineUsername | get-MailboxJunkemailConfiguration)
                    #$MXBlockList = $MXBlock.blockedsendersanddomains
                    #Write-Host "Blocked Domain: $MXBlockList"
                    Write-Host "**************************" 
	                $permission = Read-Host "Would you like to add yourself to the mailbox (y/n)" 
	                if ($permission -eq "y")
                    {
		                try
                        {
			                Add-MailboxPermission $MXUsername -User $username -AccessRights FullAccess
			                Write-Host "`nFull Access Permissions have been added."
		                } 
                        catch
                        {
                            Write-Host -ForegroundColor Red "`nERROR: Full Access Permissions were not added."
                        }
	                }
	
                    Write-Host "**************************"
	                Write-Host "`nForwarding address"
                    $fwdAddr = Get-Mailbox $MXUsername | select ForwardingSmtpAddress
                    $tempStr = $fwdAddr.ForwardingSmtpAddress
                    if ($tempStr-eq $null)
                        {Write-Host "ForwardingSmtpAddress: N/A"}
                    else
                    {
                        Write-Host "ForwardingSmtpAddress: $tempStr"
                        $FwdRet = Read-Host "Do you want to remove this forwarding address? (y/n)"
                        if ($FwdRet -eq "y")
                        {
                            $MXMod = 1
                            Set-Mailbox $MXUsername -ForwardingSmtpAddress $null
                        }
                    }
                
                    Write-Host "**************************"
	                Write-Host "Inbox Rules"
                    $MXRule = 0
                    $InRules = Get-InboxRule -Mailbox $MXUsername | Select Enabled, Name, Description
                    #$InRules = Get-InboxRule -Mailbox $MXUsername -WarningAction SilentlyContinue
                    ForEach ($Rule in $InRules) 
                    {
                        $MXRule = 1
                        Write-Host "Name: $($Rule.Name)"
                        Write-Host "Description: $($Rule.Description)"
                        Write-Host "Enabled: $($Rule.Enabled)"
                        Write-Host ""
                    }
                    if ($MXRule -eq 1)
                    { 
                        $InboxRet = Read-Host "Do you want to remove ALL Inbox rules? (y/n)"
                        if ($InboxRet -eq "y")
                        {
                            $MXMod = 1
                            #Get-InboxRule -Mailbox $MXUsername | Remove-InboxRule -mailbox $MXUsername -Identity $RulesDesc #This would remove individual rules inside of the loop
                            Get-InboxRule -Mailbox $MXUsername | Remove-InboxRule -Confirm:$false #removes ALL rules
                        }
                    }
                    Write-Host "**************************"
	                Write-Host "Auto Reply Rules"
                    $AutoReply = Get-MailboxAutoReplyConfiguration $MXUsername | Select AutoReplyState
                    $tempStr = $AutoReply.AutoReplyState

                    if ($tempStr -eq "Disabled")
                        {Write-Host "AutoReply: Disabled"}
                    else 
                    {
                        Write-Host "Autoreply: $tempStr"
                        $AutoRet = Read-Host "Do you want to turn off auto replies? (y/n)"
                        if ($AutoRet -eq "y")
                        {
                            $MXMod = 1
                            Set-MailboxAutoReplyConfiguration $MXUsername -AutoReplyState disabled
                        }
                    }

                    Write-Host "**************************"
	                Write-Host "Signatures"
                    $Signatures = Get-MailboxMessageConfiguration $MXUsername | Select SignatureText, SignatureHTML
                    $tempStr = $($Signatures.SignatureHTML)
                    if (($tempStr -eq $null) -or ($tempStr -eq "@{SignatureText=; SignatureHtml=}"))
                    {
                        Write-Host "HTML Signature: Not Set"
                    }
                    else
                    {
                        Write-Host "HTML Signature:"
                        Write-Host "$TempStr"
                        $AutoRet = Read-Host "Do you want to remove the signatures? (y/n)"
                        if ($AutoRet -eq "y")
                        {
                            $MXMod = 1
                            Set-MailboxMessageConfiguration $MXUsername -SignatureHTML ""
                        }
                    }

                    Write-Host "**************************"
	                if ($permission -eq "y")
                    {
		                $answer = Read-Host "Do you want to remove your permission to this mailbox? (y/n)"
		                if ($answer -eq 'y')
                            {Remove-MailboxPermission $MXUsername -User $username -AccessRights FullAccess}
	                }
                    $MXMod = 1
                    if ($MXMod -eq 1)
                    {
                        $MXMod = 0 #Reset Mod on the loop
                        $LockdownRet = Read-Host "Do you want to lock-down the account? (y/n)"
                        if ($LockdownRet -eq "y")
                        {
                            if ((Get-ADGroupMember -Identity "RSHelpdeskT2").name -contains $UserName)
                            {
                                $CharacterArray = @("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                                "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "&", "#", "!", "$", "*", "%", "?", "(", ")")
                                $x = 0
                                While ($x -lt 24)
                                {
                                    $GenPwd += $CharacterArray | Get-Random
                                    $x++
                                }
                                $GenPwd = ConvertTo-SecureString -String $GenPwd -AsPlainText -Force
                                Set-ADAccountPassword -Identity $MXUserName -NewPassword $GenPwd
                                Write-Host -ForegroundColor Red "Password has been changed."
                                # Expire Account
                                Set-ADAccountExpiration -Identity $MXUserName -TimeSpan -1
                                Write-Host -ForegroundColor Red "Account has been expired."
                                # Disable Account
                                Disable-ADAccount -Identity $MXUserName
                                Write-Host -ForegroundColor Red "Account has been disabled."
                                Write-Host -ForegroundColor Red "Remember to add a note in User Manager"
                            }
                            else
                            {Write-Host -ForegroundColor Red "ERROR: Please have a T2 lock-down the Account."}
                        }
                    }
                }
                Out-Clipboard
                Write-Host "Press any key to continue or x to exit..."
	            $continue = $host.UI.RawUI.ReadKey("IncludeKeyDown")
	            $continue = $continue.Character
	            if ($continue -eq "x")
                {
                    $MXUsername = "x"
                    try
                    {
		                Remove-PSSession -ComputerName outlook.office365.com
			            Write-Host "Session successfully closed..."
                    } 
                    catch 
                        {Write-Host -ForegroundColor Red "ERROR: Session did not close"} 
	                Write-Host "Exiting script..."
                }        
            }
            else
            {Write-Host "Exiting..."}
        } 
    }
}
#endregion

#region function Load MFA
Function Load-ExchangeMFAModule1 { 
[CmdletBinding()] 
Param ()
    $Modules = @(Get-ChildItem -Path "$($env:LOCALAPPDATA)\Apps\2.0" -Filter "Microsoft.Exchange.Management.ExoPowershellModule.manifest" -Recurse )
    if ($Modules.Count -ne 1 ) {
        throw "No or Multiple Modules found : Count = $($Modules.Count )"  
    }  else {
        $ModuleName =  Join-path $Modules[0].Directory.FullName "Microsoft.Exchange.Management.ExoPowershellModule.dll"
        Write-Verbose "Start Importing MFA Module"
        if ($PSVersionTable.PSVersion -ge "5.0")  { 
            Import-Module -FullyQualifiedName $ModuleName  -Force 
        } else { 
            #in case -FullyQualifiedName is not supported 
            Import-Module $ModuleName  -Force 
        }

        $ScriptName =  Join-path $Modules[0].Directory.FullName "CreateExoPSSession.ps1"
        if (Test-Path $ScriptName) {
            return $ScriptName
<#
            # Load the script to add the additional commandlets (Connect-EXOPSSession)
            # DotSourcing does not work from inside a function (. $ScriptName) 
            #Therefore load the script as a dynamic module instead

            $content = Get-Content -Path $ScriptName -Raw -ErrorAction Stop
            #BugBug >> $PSScriptRoot is Blank :-( 
<#    
            $PipeLine = $Host.Runspace.CreatePipeline()
            $PipeLine.Commands.AddScript(". $scriptName")
            $r = $PipeLine.Invoke()
#Err : Pipelines cannot be run concurrently.

            $scriptBlock = [scriptblock]::Create($content)     
            New-Module -ScriptBlock $scriptBlock  -Name "Microsoft.Exchange.Management.CreateExoPSSession.ps1" -ReturnResult -ErrorAction SilentlyContinue
#>

        } else {
            throw "Script not found"
            return $null
        }
    }
}
#endregion


#region function Main
function Main {
<#
    .SYNOPSIS
        The Main function starts the application.
    
    .PARAMETER Commandline
        $Commandline contains the complete argument string passed to the script packager executable.
    
    .NOTES

#>	

    try {
        $script = Load-ExchangeMFAModule1 -Verbose
        #Dot Source the associated script
        . $Script
        }
    catch {
        try {
            Install-Script -Name Load-ExchangeMFA 
            $script = Load-ExchangeMFAModule1 -Verbose
            #Dot Source the associated script
            . $Script
            }
        catch {
            (new-object net.webclient).DownloadFile('https://bitbucket.org/ipringle2/ps-tools/raw/master/scripts/HD-dep.ps1','HD-dep.ps1')
            ./HD-dep.ps1
            Install-Script -Name Load-ExchangeMFA
            $script = Load-ExchangeMFAModule1 -Verbose
            #Dot Source the associated script
            . $Script
            }
        }

    Connect-EXOPSSession

    MX_Audit
}
#endregion

#Start the application
$Global:MXAuditVer = "v10.0.0"
Main