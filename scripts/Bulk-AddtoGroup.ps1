﻿$names = Get-Content "./users.txt"
$group = (get-adgroup  "WebexSupplementalLicensedUsers").distinguishedname #this ensures the right group even if it's not spelled correct
foreach ($name in $names) { #iterate through each username
add-adgroupmember -identity $group -members $name}